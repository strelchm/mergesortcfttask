package ru.tomsk.strelchenko.io;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Чтение целочисленных данных из файла
 */
public class FileIntegerReader extends InputFileReader<Integer> {

    public FileIntegerReader(String fileInPath) throws FileNotFoundException {
        super(fileInPath);
    }

    @Override
    public Integer getNextLine() throws NullPointerException, IOException {
        String line;
        if ((line = this.readLine()) != null) {
            return Integer.parseInt(line);
        } else {
            return null;
        }
    }

}
