package ru.tomsk.strelchenko.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ResultFileWriter extends BufferedWriter {

    public ResultFileWriter(String fileOutPath) throws IOException {
        super(new FileWriter(fileOutPath));
    }

    public void writeLine(String s) throws IOException {
        this.write(s);
        this.newLine();
    }

}
