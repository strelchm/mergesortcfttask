package ru.tomsk.strelchenko.io;

import ru.tomsk.strelchenko.exception.WrongArgsException;
import ru.tomsk.strelchenko.sorters.FileSorter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Чтение данных из файла
 *
 * @param <T> - тип данных
 */
public abstract class InputFileReader<T> extends BufferedReader {

    public InputFileReader(String fileInPath) throws FileNotFoundException {
        super(new FileReader(fileInPath));
    }

    public abstract T getNextLine() throws IOException;

    /**
     * Фабричный метод, возвращающий читатель файла по типу данных
     */
    public static InputFileReader getFileReader(FileSorter.SortType sortType, String path) throws FileNotFoundException, WrongArgsException {
        switch (sortType) {
            case STRING:
                return new FileStringReader(path);
            case INTEGER:
                return new FileIntegerReader(path);
            default:
                throw new WrongArgsException("Undefined inputFilesSort type");
        }
    }

}
