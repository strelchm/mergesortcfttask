package ru.tomsk.strelchenko.io;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Чтение строковых данных из файла
 */
public class FileStringReader extends InputFileReader<String> {

    public FileStringReader(String fileInPath) throws FileNotFoundException {
        super(fileInPath);
    }

    @Override
    public String getNextLine() throws IOException {
        return this.readLine();
    }

}
