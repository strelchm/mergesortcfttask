package ru.tomsk.strelchenko;

import ru.tomsk.strelchenko.exception.WrongArgsException;
import ru.tomsk.strelchenko.sorters.FileSorter;
import ru.tomsk.strelchenko.sorters.IntegerFileSorter;
import ru.tomsk.strelchenko.sorters.StringFileSorter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

    private static FileSorter.SortDirection sortDirection = null;
    private static FileSorter.SortType sortType = null;

    /**
     * Точка входа
     */
    public static void main(String[] args) {
        String outputPathFile = null;
        ArrayList<String> inputPathFiles = new ArrayList<>();
        File outputFile;

        FileSorter fileSorter;

        try {
            if (args.length == 0) {
                throw new WrongArgsException("There input arguments are not found");
            }

            for (String arg : args) {
                switch (arg) {
                    case "-i":
                        checkAndSetSortType(FileSorter.SortType.INTEGER);
                        break;
                    case "-s":
                        checkAndSetSortType(FileSorter.SortType.STRING);
                        break;
                    case "-a":
                        checkAndSetSortDirection(FileSorter.SortDirection.ASC);
                        break;
                    case "-d":
                        checkAndSetSortDirection(FileSorter.SortDirection.DESC);
                        break;
                    default:
                        if (outputPathFile == null) {
                            outputPathFile = arg;
                        } else {
                            System.out.println(arg);
                            inputPathFiles.add(arg);
                        }
                        break;
                }
            }

            if (sortDirection == null) {
                sortDirection = FileSorter.SortDirection.ASC;
            }

            if (outputPathFile == null || outputPathFile.isEmpty()) {
                throw new WrongArgsException("The output file path is undefined");
            } else {
                outputFile = new File(outputPathFile);
            }

            if(outputFile.length() > 0){
                outputFile.delete();
            }


            if (inputPathFiles.size() > 0) {
                if (sortType != null) {
                    switch (sortType) {
                        case INTEGER:
                            fileSorter = new IntegerFileSorter();
                            break;
                        case STRING:
                            fileSorter = new StringFileSorter();
                            break;
                        default:
                            throw new WrongArgsException("The valueSort type is not set");
                    }
                } else {
                    throw new WrongArgsException("The valueSort type is not set");

                }

                fileSorter.inputFilesSort(sortType, sortDirection, outputFile, inputPathFiles);
            } else {
                throw new WrongArgsException("Input files are undefined");
            }

            if (fileSorter.isInterrupted()) {
                System.out.println("File sorter was interrupted. Program finished with errors and result file was written nonsucessfully");
            } else {
                System.out.println("Output file saved successfully. Program finished successfully!");
            }
        } catch (WrongArgsException | IOException | InterruptedException ex) {
            System.out.println("Program stopped with error: " + ex.getMessage());
        }
    }

    /**
     * Установление типа значений с проверкой на уже установленное значение
     *
     * @param inpurSortType
     * @throws WrongArgsException
     */
    private static void checkAndSetSortType(FileSorter.SortType inpurSortType) throws WrongArgsException {
        if (sortType == null) {
            sortType = inpurSortType;
        } else {
            throw new WrongArgsException("Error in input params: integer(-i) and string(-s) types can't set together");
        }
    }

    /**
     * Установление направления сортировки с проверкой на уже установленное значение
     *
     * @param inpurSortDirection
     * @throws WrongArgsException
     */
    private static void checkAndSetSortDirection(FileSorter.SortDirection inpurSortDirection) throws WrongArgsException {
        if (sortDirection == null) {
            sortDirection = inpurSortDirection;
        } else {
            throw new WrongArgsException("Error in input params: asc(-a) and desc(-d) directions can't set together");
        }
    }
}
