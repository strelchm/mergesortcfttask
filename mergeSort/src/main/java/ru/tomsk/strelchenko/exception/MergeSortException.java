package ru.tomsk.strelchenko.exception;

/**
 * Ошибка при неверно заданых входных параметрах
 */
public class MergeSortException extends Exception {
    public MergeSortException() {
        super();
    }

    public MergeSortException(String message) {
        super(message);
    }

    public MergeSortException(String message, Throwable cause) {
        super(message, cause);
    }

    public MergeSortException(Throwable cause) {
        super(cause);
    }
}
