package ru.tomsk.strelchenko.exception;

/**
 * Ошибка при неверно заданых входных параметрах
 */
public class WrongArgsException extends Exception {
    public WrongArgsException() {
        super();
    }

    public WrongArgsException(String message) {
        super(message);
    }

    public WrongArgsException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongArgsException(Throwable cause) {
        super(cause);
    }
}
