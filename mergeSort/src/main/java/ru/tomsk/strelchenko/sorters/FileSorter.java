package ru.tomsk.strelchenko.sorters;

import ru.tomsk.strelchenko.io.InputFileReader;
import ru.tomsk.strelchenko.io.ResultFileWriter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Базовый фасад сортировки, запускающий процессы сортивки и слияния пар файлов,
 * вплоть до записи результата в выходной файл
 *
 * @param <T> - тип данных
 */
public abstract class FileSorter<T> {
    private static final int NUMBER_OF_THREADS = 16;

    private boolean interrupted;

    /**
     * Направление сортировки
     */
    public enum SortDirection {
        ASC,        //от низких значений к высоким
        DESC        //от высоких значений к низким
    }

    /**
     * Тип данных файлов
     */
    public enum SortType {
        STRING,
        INTEGER
    }

    protected void interrupt() {
        interrupted = true;
    }

    /**
     * Сортировка пары значений с записью в файл значения-результата сортировки
     *
     * @param firstValue
     * @param secondValue
     * @param sortDirection
     * @param firstFileReader
     * @param secondFileReader
     * @param outputFile
     * @return - пара значений
     * @throws IOException
     */
    public abstract ValuePair<T> valuesPairSort(T firstValue, T secondValue, SortDirection sortDirection,
                                                InputFileReader<T> firstFileReader, InputFileReader<T> secondFileReader, ResultFileWriter outputFile) throws IOException;

    /**
     * Запуск - контроль процессов сортивки и слияния пар файлов,
     * вплоть до записи результата в выходной файл
     *
     * @param sortType
     * @param sortDirection
     * @param outputFile
     * @param inputFileNames
     * @throws InterruptedException
     * @throws IOException
     */
    public void inputFilesSort(SortType sortType, SortDirection sortDirection, File outputFile, List<String> inputFileNames) throws InterruptedException, IOException {
        int iterationsNumber = 0;
        boolean isFirstInputFiles = false;

        while (inputFileNames.size() != 1) {
            ExecutorService threadPool = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
            List<String> tempInputFileNames = new ArrayList<>();

            for (int i = 0; i < inputFileNames.size() / 2; i++) {
                String tempFileName = iterationsNumber + "_" + i + ".txt";

                threadPool.submit(new FileMergeProcess<>(this, tempInputFileNames, tempFileName,
                        sortType, sortDirection, tempFileName, inputFileNames.get(2 * i), inputFileNames.get(2 * i + 1)));
            }

            threadPool.shutdown();
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

            if (interrupted) {
                System.out.println("Warning: file sorter process was interrupted");
                return;
            }

            //При нечетном размере массива входных файлов последний файл закидываем в массив временных файлов
            if (inputFileNames.size() % 2 != 0) {
                tempInputFileNames.add(inputFileNames.get(inputFileNames.size() - 1));
            }

            //Удаление отсортированных файлов с готовыми временными файлами
            if (!isFirstInputFiles) {
                isFirstInputFiles = true;
            } else {
                for (int i = 0; i < inputFileNames.size(); i++) {
                    if (inputFileNames.size() % 2 != 0 && inputFileNames.size() != 2 && i == inputFileNames.size() - 1) {
                    } else {
                        File file = new File(inputFileNames.get(i));
                        if (!file.delete()) {
                            System.out.println("Warning: file " + inputFileNames.get(i) + " was not deleted");
                        }
                    }
                }
            }

            inputFileNames = tempInputFileNames;
            iterationsNumber++;
        }

        File sourceFile = new File(inputFileNames.get(0));

        //todo: копирование файла ниже актуально при удаленном расположении клиента, дабы не писать отдельными операциями
        //в удаленный файл. При локальном расположении файла, будет лучше сливать два последние файла в итоговый файл
        Files.copy(sourceFile.toPath(), outputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

        if (!sourceFile.delete()) {
            System.out.println("Warning: file " + sourceFile.getName() + " was not deleted");
        }
    }

    public boolean isInterrupted() {
        return interrupted;
    }
}