package ru.tomsk.strelchenko.sorters;

import ru.tomsk.strelchenko.exception.WrongArgsException;
import ru.tomsk.strelchenko.io.InputFileReader;
import ru.tomsk.strelchenko.io.ResultFileWriter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Процесс сортировки и слияния двух файлов
 *
 * @param <T> - тип данных файлов
 */
public class FileMergeProcess<T> implements Runnable {
    private FileSorter<T> fileSorter;
    private List<String> tempInputFileNames;
    private String tempFileName;
    private FileSorter.SortDirection sortDirection;
    private FileSorter.SortType sortType;
    private String outputFileName, firstFileName, secondFileName;

    public FileMergeProcess(FileSorter<T> fileSorter, List<String> tempInputFileNames, String tempFileName,
                            FileSorter.SortType sortType, FileSorter.SortDirection sortDirection, String outputFileName,
                            String firstFileName, String secondFileName) {
        this.fileSorter = fileSorter;
        this.tempInputFileNames = tempInputFileNames;
        this.tempFileName = tempFileName;
        this.sortType = sortType;
        this.sortDirection = sortDirection;
        this.outputFileName = outputFileName;
        this.firstFileName = firstFileName;
        this.secondFileName = secondFileName;
    }

    /**
     * Непосредственно процесс слияния файла с сортировкой на каждой итерации
     */
    @Override
    public void run() {
        T firstValue, secondValue;

        try (InputFileReader<T> firstInputFileReader = InputFileReader.getFileReader(sortType, firstFileName);
             InputFileReader<T> secondInputFileReader = InputFileReader.getFileReader(sortType, secondFileName);
             ResultFileWriter outputFileWriter = new ResultFileWriter(outputFileName)) {

            //начальное чтение значений
            firstValue = firstInputFileReader.getNextLine();
            secondValue = secondInputFileReader.getNextLine();

            while (firstValue != null || secondValue != null) { //пока хотя бы один из файлов не окончен
                try {
                    //конец первого файла
                    if (firstValue == null) { //конец первого файла
                        outputFileWriter.writeLine(secondValue.toString());
                        secondValue = secondInputFileReader.getNextLine();
                        continue;
                    }

                    //конец второго файла
                    if (secondValue == null) {
                        outputFileWriter.writeLine(firstValue.toString());
                        firstValue = firstInputFileReader.getNextLine();
                        continue;
                    }

                    ValuePair<T> nextIterationValuePair = fileSorter.valuesPairSort(firstValue, secondValue, sortDirection, firstInputFileReader, secondInputFileReader, outputFileWriter);
                    firstValue = nextIterationValuePair.getFirstValue();
                    secondValue = nextIterationValuePair.getSecondValue();
                } catch (IOException ex) {
                    System.out.println("Warning: error during iteration of merge process of input files " + firstFileName + ", " + secondFileName + " to output \n" +
                            "file " + outputFileName + ": " + ex.getMessage());
                }
            }

            tempInputFileNames.add(tempFileName);
        } catch (FileNotFoundException e) {
            System.out.println("Invalid file name: " + e.getMessage());
            fileSorter.interrupt();
        } catch (IOException | WrongArgsException e) {
            System.out.println(e.getMessage());
            fileSorter.interrupt();
        }

    }
}