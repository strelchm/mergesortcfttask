package ru.tomsk.strelchenko.sorters;

import ru.tomsk.strelchenko.io.InputFileReader;
import ru.tomsk.strelchenko.io.ResultFileWriter;

import java.io.IOException;

/**
 * Расширение базового фасада сортировки для целочисленного типа данных
 */
public class IntegerFileSorter extends FileSorter<Integer> {
    @Override
    public ValuePair<Integer> valuesPairSort(Integer firstValue, Integer secondValue, SortDirection sortDirection, InputFileReader<Integer> firstFileReader, InputFileReader<Integer> secondFileReader, ResultFileWriter outputFileWriter) throws IOException {
        if ((sortDirection == SortDirection.ASC && firstValue < secondValue) || (sortDirection == SortDirection.DESC && firstValue > secondValue)) {
            outputFileWriter.writeLine(String.valueOf(firstValue));
            firstValue = firstFileReader.getNextLine();
        } else {
            outputFileWriter.writeLine(String.valueOf(secondValue));
            secondValue = secondFileReader.getNextLine();
        }

        return new ValuePair<>(firstValue, secondValue);
    }
}