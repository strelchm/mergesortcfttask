package ru.tomsk.strelchenko.sorters;

import ru.tomsk.strelchenko.io.InputFileReader;
import ru.tomsk.strelchenko.io.ResultFileWriter;

import java.io.IOException;

/**
 * Расширение базового фасада сортировки для строкового типа данных
 */
public class StringFileSorter extends FileSorter<String> {

    @Override
    public ValuePair<String> valuesPairSort(String s1, String s2, SortDirection sortDirection, InputFileReader<String> firstFileReader, InputFileReader<String> secondFileReader, ResultFileWriter outputFileWriter) throws IOException {
        if ((sortDirection == SortDirection.ASC && s1.compareTo(s2) < 0) || (sortDirection == SortDirection.DESC && s1.compareTo(s2) > 0)) {
            outputFileWriter.writeLine(s1);
            s1 = firstFileReader.getNextLine();
        } else {
            outputFileWriter.writeLine(s2);
            s2 = secondFileReader.getNextLine();
        }

        return new ValuePair<>(s1, s2);
    }
}