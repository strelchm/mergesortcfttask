package ru.tomsk.strelchenko.sorters;

/**
 * Пара считываемых значений (на следующую итерацию сравнения двух считанных значений)
 *
 * @param <T> - тип значения
 */
public class ValuePair<T> {

    private T firstValue;
    private T secondValue;

    public ValuePair(T firstValue, T secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }

    public T getFirstValue() {
        return firstValue;
    }

    public T getSecondValue() {
        return secondValue;
    }
}
